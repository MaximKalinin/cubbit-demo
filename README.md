![logo](https://photos.angel.co/startups/i/7117051-7b434260f22daa95fb19409dfaed2c9d-medium_jpg.jpg)

# Cubbit Encryptor Demo Project

## Description

This is a test project that can encrypt and decrypt files.

## Usage

You could access the website [🔗here🔗](https://maximkalinin.gitlab.io/cubbit-demo/).
In order to encrypt file, just drop it to the orange zone and click "Encrypt" button.
If you want to decrypt file, place the file, click "Decrypt" button and paste the key.

### Prerequisites

You need NodeJs and Yarn / Npm installed to start this project on your machine.

### Launch

To launch locally, you need to clone this repo:
with ssh: `git clone git@gitlab.com:MaximKalinin/cubbit-demo.git`
or with https: `git clone https://gitlab.com/MaximKalinin/cubbit-demo.git`.

Then in terminal do the following:

```bash
$ yarn install && yarn start
```

Then go to http://localhost:8080/ on your browser.
You should see the page👇:
![](https://psv4.userapi.com/c856228/u121171055/docs/d4/e0b268fe2f03/maximkalinin_gitlab_io_cubbit-demo.png?extra=0lfCQtPXi7DgCH0frmYoORAdiucH2LAZNcc1zpckNMyaJ4CLt1EiapK2OumS0Uz6neEgPhdGV3bxn1fWz2cOB8CFeUyglhq8zWbJGavXlWsTQ9MHctoLeAdgfGx4JBfWMr6FYD7ZU6M)

## 🏗Working Process

_This paragraph is about the process of creating this website._

After reading the test assignment, I had some questions about posiible solutions, because I had never used Web Workers before. Also I was not sure if file could be passed to Worker without any preparations. After small investigation, I realised abilities of **Web Workers** and **Javascript File API**, it became clear that everything is possible, almost without any issues with performance.

I then quickly looked through disgn mockups, and understood, that this website needs to have a framework as it has lot of dynamic parts. So I chose **React**. I then set up a project with **Webpack** as a bundler, **Prettier** and **ESLint** to ensure the confident code style, **TypeScript** for type definitions and **Jest** for testing purposes, set up **CI**, thanks to Gitlab and their runners.

Next, I created a simple page without design to test all the functionality, and realised that the app can handle large files (hundreds of MB without any lag), so I didn't limit file size. Connection to worker was made to mimic connection to server: it's indifferent for the app, where it takes data, it just can send messages, no matter, to server or another thread.

After that, I started with designig components with **Styled Components**. I also added some transitions with **react-flip-toolkit** librabry to make the app nice and smooth.

Some problems arised when I tried to implement the Spinner component. I didn't find anything like that on the Internet, so I decided to create my own thanks to Styled Components.

After implementing **Redux** and **I18n** I realised that my app is too big. In order to reduce size, I decided to use CDN for some libraries like React, because it reduces the size of bundle, it also helps to cache a lot of code, even after the app rebuilds. Also they usually use some compression, which I can't get on Gitlab Pages, so my bundle size became even smaller.

After that I added some tests to ensure encryption process.

These were all the core steps of this task, **Thank You for reading till the end and have a great day!**
