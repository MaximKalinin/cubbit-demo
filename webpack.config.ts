import * as webpack from "webpack";
import path from "path";
import HtmlWebPackPlugin from "html-webpack-plugin";

const config: webpack.ConfigurationFactory = (_, argv) => {
  const development = argv.mode === "development";
  const htmlPlugin = new HtmlWebPackPlugin({
    react: development
      ? "https://unpkg.com/react@16/umd/react.development.js"
      : "https://unpkg.com/react@16/umd/react.production.min.js",
    reactDom: development
      ? "https://unpkg.com/react-dom@16/umd/react-dom.development.js"
      : "https://unpkg.com/react-dom@16/umd/react-dom.production.min.js",
    redux: development
      ? "https://unpkg.com/redux@4.0.5/dist/redux.js"
      : "https://unpkg.com/redux@4.0.5/dist/redux.min.js",
    template: "./index.ejs",
  });

  return {
    devtool: development ? "inline-source-map" : false,
    mode: development ? "development" : "production",
    entry: "./src/index",
    output: {
      path: path.resolve(__dirname, "public"),
      filename: development ? "bundle.js" : "bundle.[contenthash].js",
    },
    resolve: {
      // Add `.ts` and `.tsx` as a resolvable extension.
      extensions: [".ts", ".tsx", ".js"],
      alias: {
        "@app": path.resolve(__dirname, "src"),
      },
    },
    externals: {
      react: "React",
      "react-dom": "ReactDOM",
      redux: "Redux",
      i18next: "i18next",
    },
    module: {
      rules: [
        // all files with a `.ts` or `.tsx` extension will be handled by `ts-loader`
        { test: /\.tsx?$/, loader: "ts-loader" },
        {
          test: /\.(jpe?g|png|svg|(o|t)tf)$/,
          loader: "file-loader",
          options: {
            esModule: false,
          },
        },
      ],
    },
    plugins: [htmlPlugin],
  };
};

export default config;
