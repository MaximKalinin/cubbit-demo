import React from "react";
import ReactDOM from "react-dom";
import { GloblaStyle } from "@app/GlobalStyle";
import { App } from "@app/App";
import "@app/i18n";
import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import { reducer } from "@app/store/reducer";
import { Provider } from "react-redux";

const store = createStore(reducer, applyMiddleware(thunk));

ReactDOM.render(
  <Provider store={store}>
    <GloblaStyle />
    <App />
  </Provider>,
  document.getElementById("root")
);
