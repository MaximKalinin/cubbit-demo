import { ThunkDispatch as OriginalThunkDispatch } from "redux-thunk";
import { Action } from "redux";
import { IStore } from "@app/store/reducer";

export interface IDataBase {
  type: string;
}

export interface IResponseBase extends IDataBase {
  status: TWorkerStatus;
  message?: string;
}

export type TWorkerStatus = "success" | "error";

export type TRequestStatus = "not_ready" | "ready" | "error" | "loading";

export type ThunkDispatch = OriginalThunkDispatch<IStore, undefined, Action>;
