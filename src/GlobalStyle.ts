import { createGlobalStyle } from "styled-components";

export const GloblaStyle = createGlobalStyle`
  * {
    font-family: 'Nunito', sans-serif;
  }
  body {
    margin: 0;
  }
  #root {
    background: #161616;
    min-height: 100vh;
    width: 100vw;
    color: white;
  }
`;
