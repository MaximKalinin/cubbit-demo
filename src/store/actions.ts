import {
  ENCRYPT_FILE_START,
  ENCRYPT_FILE_SUCCESS,
  ENCRYPT_FILE_FAIL,
  DECRYPT_FILE_START,
  DECRYPT_FILE_SUCCESS,
  DECRYPT_FILE_FAIL,
} from "@app/store/const";
import { ICryptFileResponse, ICryptFileData } from "@app/worker/entities";
import { IResponseBase } from "@app/model";
import { Dispatch } from "redux";
import { ENCRYPT_FILE, DECRYPT_FILE } from "@app/worker/methods";
import { postWorker } from "@app/utils";

export const encryptFileStart = () => ({
  type: ENCRYPT_FILE_START,
});

export const encryptFileSuccess = (payload: ICryptFileResponse) => ({
  type: ENCRYPT_FILE_SUCCESS,
  payload,
});

export const encryptFileFail = (payload: IResponseBase) => ({
  type: ENCRYPT_FILE_FAIL,
  payload,
});

export const encryptFile = (file: File) => async (dispatch: Dispatch) => {
  dispatch(encryptFileStart());
  const data: ICryptFileData = {
    type: ENCRYPT_FILE,
    file,
  };
  try {
    const response: ICryptFileResponse = await postWorker(data);
    dispatch(encryptFileSuccess(response));
  } catch (e) {
    dispatch(encryptFileFail(e));
  }
};

export const decryptFileStart = () => ({
  type: DECRYPT_FILE_START,
});

export const decryptFileSuccess = (payload: ICryptFileResponse) => ({
  type: DECRYPT_FILE_SUCCESS,
  payload,
});

export const decryptFileFail = (payload: IResponseBase) => ({
  type: DECRYPT_FILE_FAIL,
  payload,
});

export const decryptFile = (file: File, key: string) => async (
  dispatch: Dispatch
) => {
  dispatch(decryptFileStart());
  const data: ICryptFileData = {
    type: DECRYPT_FILE,
    file,
    key,
  };
  try {
    const response: ICryptFileResponse = await postWorker(data);
    dispatch(decryptFileSuccess(response));
  } catch (e) {
    dispatch(decryptFileFail(e));
  }
};
