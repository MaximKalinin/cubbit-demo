import { TRequestStatus } from "@app/model";
import { Reducer } from "redux";
import {
  ENCRYPT_FILE_START,
  ENCRYPT_FILE_SUCCESS,
  ENCRYPT_FILE_FAIL,
  DECRYPT_FILE_START,
  DECRYPT_FILE_SUCCESS,
  DECRYPT_FILE_FAIL,
} from "@app/store/const";
import {
  encryptFileSuccess,
  encryptFileFail,
  decryptFileSuccess,
  decryptFileFail,
} from "./actions";

export interface IStore {
  downloadUrl: string;
  cryptoKey: string;
  filename: string;
  action: "encrypt" | "decrypt" | "initial";
  status: {
    action: TRequestStatus;
  };
  errors: {
    action: Error | null;
  };
}

const initialState: IStore = {
  downloadUrl: "",
  cryptoKey: "",
  filename: "",
  action: "initial",
  status: {
    action: "not_ready",
  },
  errors: {
    action: null,
  },
};

export const reducer: Reducer<IStore> = (state = initialState, action) => {
  const { errors, status } = state;
  switch (action.type) {
    case ENCRYPT_FILE_START:
      return {
        ...state,
        action: "encrypt",
        status: {
          ...status,
          action: "loading",
        },
        errors: {
          ...errors,
          action: null,
        },
      };
    case ENCRYPT_FILE_SUCCESS: {
      const {
        payload: { file, filename, key },
      } = action as ReturnType<typeof encryptFileSuccess>;
      const url = URL.createObjectURL(file);
      return {
        ...state,
        downloadUrl: url,
        cryptoKey: key,
        filename,
        status: {
          ...status,
          action: "ready",
        },
      };
    }
    case ENCRYPT_FILE_FAIL: {
      const {
        payload: { message },
      } = action as ReturnType<typeof encryptFileFail>;
      return {
        ...state,
        status: {
          ...status,
          action: "error",
        },
        errors: {
          ...errors,
          action: new Error(message),
        },
      };
    }
    case DECRYPT_FILE_START:
      return {
        ...state,
        action: "decrypt",
        status: {
          ...status,
          action: "loading",
        },
        errors: {
          ...errors,
          action: null,
        },
      };
    case DECRYPT_FILE_SUCCESS: {
      const {
        payload: { file, filename, key },
      } = action as ReturnType<typeof decryptFileSuccess>;
      const url = URL.createObjectURL(file);
      return {
        ...state,
        downloadUrl: url,
        cryptoKey: key,
        filename,
        status: {
          ...status,
          action: "ready",
        },
      };
    }
    case DECRYPT_FILE_FAIL: {
      const {
        payload: { message },
      } = action as ReturnType<typeof decryptFileFail>;
      return {
        ...state,
        status: {
          ...status,
          action: "error",
        },
        errors: {
          ...errors,
          action: new Error(message),
        },
      };
    }
    default:
      return state;
  }
};
