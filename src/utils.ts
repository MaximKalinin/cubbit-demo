import { IDataBase, IResponseBase } from "@app/model";
import Worker from "worker-loader!@app/worker";

const worker = new Worker();
if (process.env.NODE_ENV !== "production")
  worker.addEventListener("message", (event) => console.log(event));

export async function postWorker<T extends IDataBase, K extends IResponseBase>(
  data: T
): Promise<K> {
  const result: K = await new Promise((resolve) => {
    let timeoutId;
    const onClear = () => {
      worker.removeEventListener("message", onMessage);
      worker.removeEventListener("messageerror", onMessageError);
      clearTimeout(timeoutId);
    };
    const onMessage = (event: MessageEvent) => {
      const resultData: K = event.data;
      if (data.type === resultData.type) {
        onClear();
        resolve(resultData);
      }
    };
    const onMessageError = (event: MessageEvent) => {
      const initialData: T = event.data;
      if (data.type === initialData.type) {
        onClear();
        const resultData: IResponseBase = {
          type: data.type,
          message: "Can't serialize data",
          status: "error",
        };
        resolve(resultData as K);
      }
    };
    worker.addEventListener("message", onMessage);
    worker.addEventListener("messageerror", onMessageError);

    worker.postMessage(data);
  });
  if (result.status === "error") {
    throw new Error(result.message);
  }
  return result;
}
