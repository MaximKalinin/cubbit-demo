import React, { FC } from "react";
import styled from "styled-components";

interface InputProps
  extends React.DetailedHTMLProps<
    React.InputHTMLAttributes<HTMLInputElement>,
    HTMLInputElement
  > {}

const Label = styled.label`
  background: #292929;
  border-radius: 3px;
  border: 1px solid #363636;
  padding: 6px;
  input {
    border: 0;
    padding: 0;
    background: transparent;
    font-size: 1rem;
    color: white;
    margin: 6px 32px;
    &:focus {
      outline: none;
    }
  }
`;

export const Input: FC<InputProps> = (props) => {
  const { className, children, ...other } = props;
  return (
    <Label className={className}>
      <input {...other} />
      {children}
    </Label>
  );
};
