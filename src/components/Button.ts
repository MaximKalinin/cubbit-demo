import styled from "styled-components";

interface IButtonProps {
  color?: "orange" | "blue" | "lightblue";
  size?: "sm" | "md";
}

export const Button = styled.button<IButtonProps>`
  min-width: ${({ size }) => (size === "sm" ? 80 : 110)}px;
  @media (min-width: 600px) {
    min-width: ${({ size }) => (size === "sm" ? 110 : 170)}px;
  }
  cursor: pointer;
  border: none;
  border-radius: 3px;
  background: ${({ color }) => getColor(color)};
  color: white;
  font-size: 16px;
  padding: 10px;
`;

Button.defaultProps = {
  size: "md",
  color: "lightblue",
};

function getColor(color: IButtonProps["color"]) {
  switch (color) {
    case "blue":
      return "#0065FF";
    case "lightblue":
      return "#009EFF";
    case "orange":
      return "#FFA047";
    default:
      return "";
  }
}
