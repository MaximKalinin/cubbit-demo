import React, { FC } from "react";
import styled, { keyframes } from "styled-components";
import { Files } from "@app/assets/files";
import arrowDown from "@app/assets/arrow_down.svg";
import { useTranslation } from "react-i18next";
import { useDropzone } from "react-dropzone";
import { Flipper, Flipped } from "react-flip-toolkit";

const scaleIn = keyframes`
  0% {
    opacity: 0;
    transform: scale(0.3);
  }
  100% {
    opacity: 1;
    transform: scale(1);
  }
`;

const Wrapper = styled.div`
  width: 100%;
  box-sizing: border-box;
  max-width: 900px;
  .flip {
    background: #ffa047;
    padding: 8px;
    transition-property: background;
    transition-duration: 0.3s;
  }
  &.readonly {
    max-width: 500px;
    .flip {
      padding: 0px;
      background: transparent;
      .inner {
        background: transparent;
        border: 1px solid #363636;
        color: white;
        padding: 16px 0;
      }
    }
  }
  .inner {
    background: rgba(22, 22, 22, 0.16);
    transition-property: background, border;
    transition-duration: 0.3s;
    border: 1px dashed #363636;
    padding: 64px 0 40px 0;
    color: black;
    display: flex;
    flex-direction: column;
    align-items: center;
    &.active {
      background: rgba(22, 22, 22, 0.4);
    }
  }
  .input {
    cursor: pointer;
    max-width: 260px;
    width: 100%;
    &:hover {
      background: #d1d1d1;
    }
    background: #ffffff;
    transition: background 0.3s;
    border-radius: 3px;
    display: flex;
    & > img,
    span {
      margin: 12px 0;
    }
    & > img {
      margin-left: 24px;
      display: block;
      margin-right: 30px;
    }
    span {
      flex-grow: 1;
    }
    div {
      border-left: 1px solid #98a0a6;
      padding: 12px;
      display: flex;
      align-items: center;
    }
  }
  .drop-label {
    padding-top: 24px;
    text-align: center;
    display: block;
  }
  .file-wrapper {
    display: flex;
    flex-direction: column;
    align-items: center;
    .file-image {
      svg {
        height: 48px;
        width: 48px;
      }
      animation: ${scaleIn} 500ms;
    }
    span {
      padding-top: 16px;
    }
  }
`;

interface IDropZoneProps {
  onChange: (file: File | undefined) => void;
  file: File | undefined;
  readonly?: boolean;
}

export const DropZone: FC<IDropZoneProps> = (props) => {
  const { onChange, file, readonly } = props;
  const { t } = useTranslation();
  const processFiles = (files: FileList | File[]) => {
    const arrFiles = Array.isArray(files) ? files : Array.from(files);
    if (files.length === 0) {
      onChange(undefined);
      return;
    }
    onChange(arrFiles[0]);
  };
  const { getRootProps, getInputProps, isDragActive } = useDropzone({
    onDrop: processFiles,
  });
  const content = file ? (
    <div className="file-wrapper">
      <Files className="file-image" fill={readonly ? "#FFFFFF" : "#292929"} />
      {/* <img className="file-image" src={filesImg} /> */}
      <span>{file.name}</span>
    </div>
  ) : (
    <>
      <div className="input">
        <Files fill={"#292929"} />
        <span>{t("fileInput.label")}</span>
        <div>
          <img src={arrowDown} />
        </div>
        <input
          {...getInputProps({
            multiple: false,
          })}
        />
      </div>
      <span className="drop-label">{t("fileInput.dropLabel")}</span>
    </>
  );
  return (
    <Wrapper className={readonly ? " readonly" : ""}>
      <Flipper flipKey={readonly}>
        <Flipped flipId="drop-zone">
          <div className={"flip"}>
            <div
              className={`inner${isDragActive ? " active" : ""}`}
              {...getRootProps()}
            >
              {content}
            </div>
          </div>
        </Flipped>
      </Flipper>
    </Wrapper>
  );
};
