import React, { FC, useMemo } from "react";
import styled from "styled-components";
import { Switcher } from "@app/components";
import logo from "@app/assets/logo.svg";
import { LANGUAGES } from "@app/const";
import { useTranslation } from "react-i18next";

const Wrapper = styled.header`
  background: #292929;
  padding: 13px 23px;
  position: relative;
  margin-bottom: 84px;
  .switcher-wrapper {
    position: absolute;
    display: flex;
    align-items: center;
    justify-content: center;
    padding-top: 24px;
    width: 100%;
    bottom: 0;
    left: 0;
    transform: translateY(100%);
    @media (min-width: 600px) {
      transform: initial;
      height: 100%;
      right: 160px;
      width: auto;
      left: auto;
    }
  }
`;

export const Menu: FC = () => {
  const { t, i18n } = useTranslation();
  t("hello");
  const [activeLanguageId, langOptions] = useMemo(() => {
    const langOptions = LANGUAGES.map(({ key }) => t(key)) as [string, string];
    const activeLanguageId = LANGUAGES.findIndex(
      ({ value }) => value === i18n.language
    );
    return [activeLanguageId, langOptions];
  }, [i18n.language]);
  return (
    <Wrapper>
      <img src={logo} alt="logo" />
      <div className="switcher-wrapper">
        <Switcher
          options={langOptions}
          activeOptionId={activeLanguageId}
          onOptionClick={(_, id) => i18n.changeLanguage(LANGUAGES[id].value)}
        />
      </div>
    </Wrapper>
  );
};
