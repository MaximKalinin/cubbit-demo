import React, { FC } from "react";
import styled from "styled-components";

interface IWrapperProps {
  backdropIndex: number;
}

const PADDING = 2;

const Wrapper = styled.div<IWrapperProps>`
  background: #009eff;
  border-radius: 2px;
  padding: ${PADDING}px;
  display: inline-block;
  position: relative;
  .option {
    padding: 6px 20px;
    box-sizing: border-box;
    width: 110px;
    text-align: center;
    display: inline-block;
    cursor: pointer;
    position: relative;
    z-index: 1;
  }
  .backdrop {
    position: absolute;
    width: calc(50% - ${PADDING}px);
    bottom: ${PADDING}px;
    background: #363636;
    border-radius: 2px;
    top: ${PADDING}px;
    left: ${PADDING}px;
    transform: ${({ backdropIndex }) => `translateX(${backdropIndex * 100}%)`};
    transition: transform 0.3s;
  }
`;

interface ISwitcherProps {
  options: [string, string];
  onOptionClick?: (option: string, optionId: number) => void;
  activeOptionId: number;
}

export const Switcher: FC<ISwitcherProps> = (props) => {
  const { options, onOptionClick, activeOptionId } = props;
  return (
    <Wrapper backdropIndex={activeOptionId}>
      <span className="backdrop" />
      {options.map((option, index) => (
        <span
          className="option"
          key={option}
          onClick={() => onOptionClick(option, index)}
        >
          {option}
        </span>
      ))}
    </Wrapper>
  );
};
