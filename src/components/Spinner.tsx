import React, { FC } from "react";
import styled, { css, keyframes } from "styled-components";

const spans = [0, 1, 2, 3, 4, 5, 6, 7, 8];

const TIMING = 3000;

const createAnim = (index: number) => keyframes`
  from {
    transform: translate(0, 0);
  }
  16% {
    transform: translate(0, 0);
  }
  33% {
    transform: translate(${(index % 3) * 150 - 150}%, ${
  Math.trunc(index / 3) * 150 - 150
}%);
  }
  50% {
    transform: translate(0, 0);
  }
  67% {
    transform: translate(0, 0);
  }
  84% {
    transform: translate(0, 0);
  }
  to {
    transform: translate(0, 0);
  }
`;

const wrapperAnim = keyframes`
  from {
    transform: scale(0.5);
  }
  16% {
    transform: scale(1);
  }
  33% {
    transform: scale(1);
  }
  50% {
    transform: scale(1);
  }
  67% {
    transform: scale(0.5);
  }
  84% {
    transform: scale(1.5); 
  }
  to {
    transform: scale(0.5);
  }
`;

const SIZE = 40;

const Wrapper = styled.span`
  display: flex;
  flex-wrap: wrap;
  width: ${SIZE}px;
  height: ${SIZE}px;
  animation: ${wrapperAnim} ${TIMING}ms infinite;
  span {
    background: white;
    width: ${SIZE / 3 + 1}px;
    height: ${SIZE / 3 + 1}px;
    margin: -1px;
    display: inline-block;
    background: #ffa047;
  }
  ${spans.map(
    (index) => css`
      span:nth-child(${index + 1}) {
        transform-origin: ${150 - (index % 3) * 100}%
          ${150 - Math.trunc(index / 3) * 100}%;
        animation: ${createAnim(index)} ${TIMING}ms infinite;
      }
    `
  )}
  span:nth-child(1) {
  }
`;

export const Spinner: FC<{ className?: string }> = ({ className }) => (
  <Wrapper className={className}>
    {spans.map((index) => (
      <span key={index} />
    ))}
  </Wrapper>
);
