import React, { FC, useState, useEffect, useRef } from "react";
import { Menu, DropZone, Button, Spinner, Input } from "@app/components";
import { useTranslation } from "react-i18next";
import styled from "styled-components";
import { connect } from "react-redux";
import { IStore } from "./store/reducer";
import { encryptFile, decryptFile } from "@app/store/actions";
import { ThunkDispatch } from "@app/model";
import throttle from "lodash.throttle";

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  .centered {
    text-align: center;
    word-break: break-word;
  }
  .menu-wrapper {
    align-self: stretch;
  }
  .button-wrapper {
    display: flex;
    justify-content: space-between;
    margin: 48px 8px 0 8px;
    align-self: stretch;
    @media (min-width: 600px) {
      width: 360px;
      align-self: center;
    }
    .button {
      flex: 1 1 auto;
      margin-left: 12px;
      &:first-child {
        margin-left: 0;
        margin-right: 12px;
      }
    }
  }
  .input {
    max-width: 500px;
    width: 80%;
    box-sizing: border-box;
    position: relative;
    .inside-button {
      top: 2px;
      right: 2px;
      position: absolute;
    }
  }
  .footer {
    margin-top: 60px;
    color: #98a0a6;
    &.absolute {
      position: absolute;
      bottom: 0;
    }
  }
  .spinner {
    margin-top: 60px;
  }
`;

export const AppEl: FC<
  ReturnType<typeof mapStateToProps> & ReturnType<typeof mapDispatchToProps>
> = (props) => {
  const {
    action,
    downloadUrl,
    filename,
    cryptoKey,
    status: { action: actionStatus },
    errors: { action: actionError },
    encryptFile,
    decryptFile,
  } = props;
  const { t } = useTranslation();
  const [file, setFile] = useState<File>();
  const [result, setResult] = useState({ href: "", name: "", key: "" });
  const [stage, setStage] = useState<"initial" | "decrypt" | "encrypt">(
    "initial"
  );
  const ref = useRef<HTMLDivElement>();
  const [isFooterAbsolute, setIsFooterAbsolute] = useState(true);
  const isLoading = actionStatus === "loading";
  const isDecryptStage = stage === "decrypt";
  useEffect(() => {
    if (actionStatus === "ready") {
      setResult({ href: downloadUrl, name: filename, key: cryptoKey });
      if (action === "encrypt") {
        setStage("encrypt");
      } else if (action === "decrypt") {
        const a = document.createElement("a");
        a.style.display = "none";
        a.href = downloadUrl;
        a.download = filename;
        document.body.appendChild(a);
        a.click();
        window.URL.revokeObjectURL(downloadUrl);
        document.body.removeChild(a);
      }
      return;
    }
  }, [actionStatus]);
  useEffect(() => {
    const onResize = throttle(() => {
      const wrapperEl = ref.current;
      const footerEl = document.getElementsByClassName("footer").item(0);
      if (!wrapperEl || !footerEl) return;
      if (wrapperEl.clientHeight + footerEl.clientHeight > window.innerHeight) {
        setIsFooterAbsolute(false);
      } else {
        setIsFooterAbsolute(true);
      }
    }, 200);
    window.addEventListener("resize", onResize);
    return () => window.removeEventListener("resize", onResize);
  }, []);
  const createOnClick = (
    type: "encrypt" | "decrypt" | "decrypt-confirm"
  ) => () => {
    if (!file) return;
    const isEncrypt = type === "encrypt";
    if (isEncrypt) {
      encryptFile(file);
      return;
    }
    if (type === "decrypt-confirm") {
      if (!result.key) return;
      decryptFile(file, result.key);
      return;
    }
    setStage("decrypt");
    return;
  };
  let buttons;
  if (isDecryptStage) {
    buttons = (
      <Button
        disabled={Boolean(!file)}
        onClick={createOnClick("decrypt-confirm")}
        className="button"
        color="orange"
      >
        {t("button.decryptAndDownload")}
      </Button>
    );
  } else if (stage === "encrypt" && actionStatus === "ready") {
    buttons = (
      <a href={result.href} download={result.name} style={{ margin: "auto" }}>
        <Button disabled={Boolean(!file)} className="button" color="orange">
          {t("button.download")}
        </Button>
      </a>
    );
  } else {
    buttons = (
      <>
        <Button
          disabled={Boolean(!file)}
          onClick={createOnClick("encrypt")}
          className="button"
        >
          {t("button.encrypt")}
        </Button>
        <Button
          disabled={Boolean(!file)}
          onClick={createOnClick("decrypt")}
          className="button"
          color="blue"
        >
          {t("button.decrypt")}
        </Button>
      </>
    );
  }
  let input;
  const isEncryptAndReady = stage === "encrypt" && actionStatus === "ready";
  if (isDecryptStage || isEncryptAndReady) {
    input = (
      <>
        <p>{t("keyInput.label")}</p>
        <Input
          className="input"
          type="text"
          value={result.key}
          placeholder={t("keyInput.placeholder")}
          onChange={(e) => {
            setResult({ ...result, key: e.target.value });
          }}
        >
          {isEncryptAndReady ? (
            <Button
              color="lightblue"
              size="sm"
              className="inside-button"
              onClick={() => navigator.clipboard.writeText(result.key)}
            >
              {t("button.copy")}
            </Button>
          ) : null}
        </Input>
      </>
    );
  }
  const content = (
    <>
      <p className="centered">{t("common.description")}</p>
      {actionError && <p className="centered">{actionError.message}</p>}
      <DropZone file={file} onChange={setFile} readonly={stage !== "initial"} />
      {input}
      <div className="button-wrapper">{buttons}</div>
    </>
  );
  return (
    <Wrapper ref={ref as any}>
      <div className="menu-wrapper">
        <Menu />
      </div>
      {/* <Input className="input" type="text" placeholder="encryption key">
        <Button color="lightblue" size="sm" className="inside-button">
          {t("button.copy")}
        </Button>
      </Input> */}
      <h1 className="centered">{t("common.header")}</h1>
      {isLoading ? <Spinner className="spinner" /> : content}
      <footer className={`footer${isFooterAbsolute ? " absolute" : ""}`}>
        <p className="centered">{t("common.afterText")}</p>
      </footer>
    </Wrapper>
  );
};

const mapStateToProps = (state: IStore) => state;

const mapDispatchToProps = (dispatch: ThunkDispatch) => ({
  encryptFile: (file: File) => dispatch(encryptFile(file)),
  decryptFile: (file: File, key: string) => dispatch(decryptFile(file, key)),
});

export const App = connect(mapStateToProps, mapDispatchToProps)(AppEl);
