import { resources } from "@app/i18n/index";

export interface ILanguage {
  key: string;
  value: keyof typeof resources;
}
export const LANGUAGES: [ILanguage, ILanguage] = [
  { key: "switcher.encrypted", value: "encrypted" },
  { key: "switcher.english", value: "english" },
];
