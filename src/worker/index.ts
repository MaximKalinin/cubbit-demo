import { IDataBase } from "@app/model";
import {
  LONG_TASK,
  SHORT_TASK,
  ENCRYPT,
  DECRYPT,
  ENCRYPT_FILE,
  DECRYPT_FILE,
} from "@app/worker/methods";
import {
  longTask,
  ILongTaskData,
  shortTask,
  IShortTaskData,
  encrypt,
  IEncryptData,
  decrypt,
  processFile,
  ICryptFileData,
} from "@app/worker/entities";

const ctx: Worker = self as any;

// Respond to message from parent thread
ctx.addEventListener("message", handler);

async function handler(message: MessageEvent) {
  const data: IDataBase = message.data;
  let result;
  switch (data.type) {
    case LONG_TASK:
      result = longTask(data as ILongTaskData);
      break;
    case SHORT_TASK:
      result = shortTask(data as IShortTaskData);
      break;
    case ENCRYPT:
      result = encrypt(data as IEncryptData);
      break;
    case DECRYPT:
      result = decrypt(data as IEncryptData);
      break;
    case ENCRYPT_FILE:
      result = await processFile("encrypt", data as ICryptFileData);
      break;
    case DECRYPT_FILE:
      result = await processFile("decrypt", data as ICryptFileData);
      break;
    default:
      break;
  }
  ctx.postMessage(result);
}
