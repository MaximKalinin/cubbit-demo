import { IDataBase, IResponseBase } from "@app/model";

export const BOUNDS = {
  min: 32,
  max: 125,
};

export function encryptHandler(key: string, message: string) {
  if (!message || !key) {
    throw new Error(
      `expected key and message to be truthy, but got: key = ${JSON.stringify(
        key
      )}; message = ${JSON.stringify(message)}`
    );
  }
  const splits = splitStringByLength(message, key.length);
  const reversedSplits = splits.map(reverseString);
  const shift = key
    .split("")
    .reduce((acc, char) => acc + char.charCodeAt(0), 0);
  const shiftedSplits = reversedSplits.map((subString) =>
    subString
      .split("")
      .map((char) =>
        String.fromCharCode(
          shiftInBoundaries(char.charCodeAt(0), shift, BOUNDS.max, BOUNDS.min)
        )
      )
      .join("")
  );
  return shiftedSplits.map(reverseString).join("");
}

export function decryptHandler(key: string, message: string) {
  if (!message || !key) {
    throw new Error(
      `expected key and message to be truthy, but got: key = ${JSON.stringify(
        key
      )}; message = ${JSON.stringify(message)}`
    );
  }
  const splits = splitStringByLength(message, key.length);
  const reversedSplits = splits.map(reverseString);
  const shift = key
    .split("")
    .reduce((acc, char) => acc + char.charCodeAt(0), 0);
  const unshiftedSplits = reversedSplits.map((subString) =>
    subString
      .split("")
      .map((char) =>
        String.fromCharCode(
          unshiftInBoundaries(char.charCodeAt(0), shift, BOUNDS.max, BOUNDS.min)
        )
      )
      .join("")
  );
  return unshiftedSplits.map(reverseString).join("");
}

function splitStringByLength(str: string, length: number) {
  return str.match(new RegExp(`.{1,${length}}`, "g"));
}

function reverseString(str: string) {
  return str.split("").reverse().join("");
}

function shiftInBoundaries(num: number, shift: number, max: number, min = 0) {
  let output = num + shift;
  const diff = max - min + 1;
  while (output > max) {
    output = output - diff;
  }
  return output;
}

function unshiftInBoundaries(num: number, shift: number, max: number, min = 0) {
  let output = num - shift;
  const diff = max - min + 1;
  while (output < min) {
    output = output + diff;
  }
  return output;
}

export interface IEncryptData extends IDataBase {
  key: string;
  message: string;
}

export interface IEncryptResponse extends IResponseBase {
  encryptedMessage: string;
}

export function encrypt(data: IEncryptData): IEncryptResponse {
  const { key, message } = data;
  const encryptedMessage = encryptHandler(key, message);
  const response: IEncryptResponse = {
    type: data.type,
    encryptedMessage,
    status: "success",
  };
  return response;
}

export interface IDecryptResponse extends IResponseBase {
  decryptedMessage: string;
}

export function decrypt(data: IEncryptData): IDecryptResponse {
  const { key, message } = data;
  const decryptedMessage = decryptHandler(key, message);
  const response: IDecryptResponse = {
    type: data.type,
    decryptedMessage,
    status: "success",
  };
  return response;
}

// ["titanic", "minecraft", "javascript"].map((message) =>
//   console.log(encryptHandler("frontend", message))
// );
// [
//   'U)tt{(2w"u&-$(#&',
//   'Sv*s"uwv2#" {"w2x{ w2w"u&-$({#"2s"v2vwu&-$({#"@2ewu)&w2s"-2x{ w2(-$w2s"v2!s{"(s{"2-#)&2$&{*su-3',
//   "Uz##'w2x{ w3",
//   "fzw2+z# w2{'2\"w*w&2(zw2')!2#x2(zw2$s&('2?2{(2{'2y&ws(w&2#&2 w''w&>2vw$w\"v{\"y2#\"2z#+2+w  2(zw2{\"v{*{v)s '2+#&}2(#yw(zw&",
//   "W\"y {'z",
//   '["\'w&(2w"u&-$({#"2}w-',
//   "pTBydX1OncWG62XXgOifP",
// ].map((msg) => console.log(decryptHandler("frontend", msg)));
