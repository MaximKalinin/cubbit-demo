import { IDataBase, IResponseBase } from "@app/model";

export interface ILongTaskData extends IDataBase {
  name: string;
}

export interface ILongTaskResponse extends IResponseBase {
  greeting: string;
}

export function longTask(data: ILongTaskData): ILongTaskResponse {
  const now = performance.now();
  while (now + 5000 > performance.now()) {
    let i = 0;
  }
  const response: ILongTaskResponse = {
    type: data.type,
    status: "success",
    greeting: `long task is finished, ${data.name}`,
  };
  return response;
}
