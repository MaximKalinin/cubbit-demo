import { IDataBase, IResponseBase } from "@app/model";

export interface IShortTaskData extends IDataBase {
  name: string;
}

export interface IShortTaskResponse extends IResponseBase {
  greeting: string;
}

export function shortTask(data: IShortTaskData): IShortTaskResponse {
  const now = performance.now();
  while (now + 100 > performance.now()) {
    let i = 0;
  }
  const response: IShortTaskResponse = {
    type: data.type,
    status: "success",
    greeting: `short task is finished, ${data.name}`,
  };
  return response;
}
