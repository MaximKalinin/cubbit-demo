import { IDataBase, IResponseBase } from "@app/model";
import { encryptHandler, BOUNDS, decryptHandler } from "@app/worker/entities";
import { nanoid } from "nanoid";

export interface ICryptFileData extends IDataBase {
  file: File;
  key?: string;
}

export interface ICryptFileResponse extends IResponseBase {
  key: string;
  file: Blob;
  filename: string;
}

export async function processFile(
  method: "decrypt" | "encrypt",
  data: ICryptFileData
): Promise<IResponseBase | ICryptFileResponse> {
  const { file, type, key } = data;
  const isEncrypt = method === "encrypt";
  const cryptoFunc = isEncrypt ? encryptHandler : decryptHandler;
  const fileText = await readFile(file);
  const errorMessage = validateString(fileText);
  if (errorMessage) {
    const response: IResponseBase = {
      status: "error",
      type,
      message: errorMessage,
    };
    return response;
  }
  if (!isEncrypt && !key) {
    const response: IResponseBase = {
      status: "error",
      type,
      message: "no key was passed",
    };
    return response;
  }
  const internalKey = isEncrypt ? key || generateKey() : key;
  try {
    const encryptedText = cryptoFunc(internalKey, fileText);
    const encryptedFile = new Blob([encryptedText], { type: file.type });
    const response: ICryptFileResponse = {
      status: "success",
      type,
      file: encryptedFile,
      filename: file.name,
      key: internalKey,
    };
    return response;
  } catch (e) {
    const response: IResponseBase = {
      status: "error",
      type,
      message: e.message,
    };
    return response;
  }
}

async function readFile(file: Blob): Promise<string> {
  return new Promise((resolve) => {
    const fileReader = new FileReader();
    const onFileRead = async (e: ProgressEvent<FileReader>) => {
      fileReader.removeEventListener("load", onFileRead);
      resolve(e.target.result as string);
    };
    fileReader.addEventListener("load", onFileRead);
    fileReader.readAsText(file);
  });
}

function validateString(str: string) {
  let errorMessage = "";
  str.split("").some((char, index) => {
    const charCode = char.charCodeAt(0);
    const isValid = charCode >= BOUNDS.min && charCode <= BOUNDS.max;
    if (!isValid) {
      errorMessage = `file contains forbidden symbol at position ${index} (${JSON.stringify(
        char
      )}). It can only accept ASCII symbols from 32 (" ") to 125 ("}") included`;
    }
    return !isValid;
  });
  return errorMessage;
}

function generateKey() {
  return nanoid(21);
}
