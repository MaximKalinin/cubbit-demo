export const LONG_TASK = "LONG_TASK";
export const SHORT_TASK = "SHORT_TASK";
export const ENCRYPT = "ENCRYPT";
export const DECRYPT = "DECRYPT";
export const ENCRYPT_FILE = "ENCRYPT_FILE";
export const DECRYPT_FILE = "DECRYPT_FILE";
