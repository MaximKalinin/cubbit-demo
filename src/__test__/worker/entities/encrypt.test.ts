import { encryptHandler, decryptHandler } from "@app/worker/entities";

describe("encryptHandler", () => {
  it("should throw an error if key is empty", () => {
    expect.assertions(1);
    try {
      encryptHandler("", "basd");
    } catch (e) {
      expect(e).toBeInstanceOf(Error);
    }
  });
  it("should throw an error if message is empty", () => {
    expect.assertions(1);
    try {
      encryptHandler("asdsad", "");
    } catch (e) {
      expect(e).toBeInstanceOf(Error);
    }
  });
  it("should encrypt message correctly", () => {
    expect(encryptHandler("frontend", "titanic")).toBe('({(s"{u');
  });
});

describe("decryptHandler", () => {
  it("should throw an error if key is empty", () => {
    expect.assertions(1);
    try {
      decryptHandler("", "basd");
    } catch (e) {
      expect(e).toBeInstanceOf(Error);
    }
  });
  it("should throw an error if message is empty", () => {
    expect.assertions(1);
    try {
      decryptHandler("asdsad", "");
    } catch (e) {
      expect(e).toBeInstanceOf(Error);
    }
  });
  it("should decrypt message correctly", () => {
    expect(decryptHandler("frontend", 'U)tt{(2w"u&-$(#&')).toBe(
      "Cubbit encryptor"
    );
  });
});
