import { processFile } from "@app/worker/entities";
import { ENCRYPT_FILE } from "@app/worker/methods";

describe("processFile", () => {
  it("should return error status if file is empty", () => {
    processFile("encrypt", {
      type: ENCRYPT_FILE,
      file: new File([new Blob([""])], "name"),
      key: "frontend",
    }).then((response) => {
      expect(response.status).toBe("error");
    });
  });
  it("should return file if everything is OK", () => {
    processFile("encrypt", {
      type: ENCRYPT_FILE,
      file: new File([new Blob(["titanic"])], "name"),
      key: "frontend",
    }).then((response) => {
      expect("file" in response).toBeTruthy();
    });
  });
});
