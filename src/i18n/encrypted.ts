export default {
  translation: {
    switcher: {
      english: "W\"y {'z",
      encrypted: "Encrypted",
    },
    common: {
      header: 'U)tt{(2w"u&-$(#&',
      description:
        'Sv*s"uwv2#" {"w2x{ w2w"u&-$({#"2s"v2vwu&-$({#"@2ewu)&w2s"-2x{ w2(-$w2s"v2!s{"(s{"2-#)&2$&{*su-3',
      afterText:
        "fzw2+z# w2{'2\"w*w&2(zw2')!2#x2(zw2$s&('2?2{(2{'2y&ws(w&2#&2 w''w&>2vw$w\"v{\"y2#\"2z#+2+w  2(zw2{\"v{*{v)s '2+#&}2(#yw(zw&",
    },
    fileInput: {
      label: "Uz##'w2x{ w3",
      dropLabel: "or drop files here",
    },
    keyInput: {
      label: "Your encryption key:",
      placeholder: '["\'w&(2w"u&-$({#"2}w-',
    },
    button: {
      encrypt: "Encrypt",
      decrypt: "Decrypt",
      copy: "Copy",
      download: "Download",
      decryptAndDownload: "Decrypt and download",
    },
  },
};
