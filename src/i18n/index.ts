import i18n from "i18next";
import { initReactI18next } from "react-i18next";
import encrypted from "@app/i18n/encrypted";
import english from "@app/i18n/english";

export const resources = {
  english,
  encrypted,
};

i18n.use(initReactI18next).init({
  resources,
  lng: "encrypted",
  fallbackLng: "encrypted",
  interpolation: {
    escapeValue: false,
  },
});

export default i18n;
