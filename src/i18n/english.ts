export default {
  translation: {
    switcher: {
      english: "English",
      encrypted: "Encrypted",
    },
    common: {
      header: "Cubbit encryptor",
      description:
        "Advanced online file encryption and decryption. Secure any file type and maintain your privacy!",
      afterText:
        "The whole is never the sum of the parts - it is greater or lesser, depending on how well the individuals work together",
    },
    fileInput: {
      label: "Choose file!",
      dropLabel: "or drop files here",
    },
    keyInput: {
      label: "Your encryption key:",
      placeholder: "Insert encryption key",
    },
    button: {
      encrypt: "Encrypt",
      decrypt: "Decrypt",
      copy: "Copy",
      download: "Download",
      decryptAndDownload: "Decrypt and download",
    },
  },
};
